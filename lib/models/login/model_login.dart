class ValueDataLogin {
  MyDataLogin value;

  ValueDataLogin({this.value});

  ValueDataLogin.fromJson(Map<String, dynamic> json) {
    value = json['value'] != null ? new MyDataLogin.fromJson(json['value']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.value != null) {
      data['value'] = this.value.toJson();
    }
    return data;
  }
}

class MyDataLogin {
  bool success;
  String message;
  String token;
  int currUser;
  int role;
  String userName;
  String no_hp;
  String foto;
  String alamat;
  String email;

  MyDataLogin(
      {this.success,
      this.message,
      this.token,
      this.currUser,
      this.role,
      this.userName,
      this.no_hp,
      this.foto,
      this.alamat,
      this.email
      });

  MyDataLogin.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    token = json['token'];
    currUser = json['currUser'];
    role = json['role'];
    userName = json['userName'];
    no_hp = json['no_hp'];
    foto = json['foto'];
    alamat = json['alamat'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    data['token'] = this.token;
    data['currUser'] = this.currUser;
    data['role'] = this.role;
    data['userName'] = this.userName;
    data['no_hp'] = this.no_hp;
    data['foto'] = this.foto;
    data['alamat']=this.alamat;
    data['email']= this.email;
    return data;
  }
}