class ModelKategori {
  int status;
  List<ValuesKategori> values;

  ModelKategori({this.status, this.values});

  ModelKategori.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['values'] != null) {
      values = new List<ValuesKategori>();
      json['values'].forEach((v) {
        values.add(new ValuesKategori.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.values != null) {
      data['values'] = this.values.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ValuesKategori {
  int id;
  String namaKategori;

  ValuesKategori({this.id, this.namaKategori});

  ValuesKategori.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    namaKategori = json['nama_kategori'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nama_kategori'] = this.namaKategori;
    return data;
  }
}

class DropModelKategori {
  String namaKategori;
  int id;

  DropModelKategori(this.namaKategori, this.id);

  @override
  String toString() {
    return '{ ${this.namaKategori}, ${this.id} }';
  }
}