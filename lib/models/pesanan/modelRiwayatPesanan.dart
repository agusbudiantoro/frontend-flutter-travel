class ModelRiwayatPesanan {
  int metodePembayaran;
  int idKonsumen;
  int idKeranjang;
  String waktuPengiriman;
  String namaPenerima;
  String alamat;
  String noHp;
  int idBarang;
  String namaBarang;
  int qty;
  int harga;
  int totalHarga;
  String waktuPemesanan;
  String estimasiSampai;

  ModelRiwayatPesanan(
      {this.metodePembayaran,
      this.idKeranjang,
      this.idKonsumen,
      this.waktuPengiriman,
      this.namaPenerima,
      this.alamat,
      this.noHp,
      this.idBarang,
      this.namaBarang,
      this.qty,
      this.harga,
      this.totalHarga,
      this.waktuPemesanan,
      this.estimasiSampai});

  ModelRiwayatPesanan.fromJson(Map<String, dynamic> json) {
    metodePembayaran = json['metode_pembayaran'];
    idKonsumen = json['id_konsumen'];
    waktuPengiriman = json['waktu_pengiriman'];
    namaPenerima = json['nama_penerima'];
    alamat = json['alamat'];
    noHp = json['no_hp'];
    idBarang = json['id_barang'];
    namaBarang = json['nama_barang'];
    qty = json['qty'];
    harga = json['harga'];
    totalHarga = json['total_harga'];
    waktuPemesanan = json['waktu_pemesanan'];
    estimasiSampai = json['estimasi_sampai'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['metode_pembayaran'] = this.metodePembayaran;
    data['id_konsumen'] = this.idKonsumen;
    data['waktu_pengiriman'] = this.waktuPengiriman;
    data['nama_penerima'] = this.namaPenerima;
    data['alamat'] = this.alamat;
    data['no_hp'] = this.noHp;
    data['id_barang'] = this.idBarang;
    data['nama_barang'] = this.namaBarang;
    data['qty'] = this.qty;
    data['harga'] = this.harga;
    data['total_harga'] = this.totalHarga;
    data['waktu_pemesanan'] = this.waktuPemesanan;
    data['estimasi_sampai'] = this.estimasiSampai;
    return data;
  }
}