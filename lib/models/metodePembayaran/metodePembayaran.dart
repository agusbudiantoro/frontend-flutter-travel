class metodePembayaranModel {
  int status;
  List<ValuesMetodePembayaranModel> values;

  metodePembayaranModel({this.status, this.values});

  metodePembayaranModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['values'] != null) {
      values = new List<ValuesMetodePembayaranModel>();
      json['values'].forEach((v) {
        values.add(new ValuesMetodePembayaranModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.values != null) {
      data['values'] = this.values.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ValuesMetodePembayaranModel {
  int idMetodePembayaran;
  String namaPembayaran;
  String logo;
  bool statusPilih = false;

  ValuesMetodePembayaranModel({this.idMetodePembayaran, this.namaPembayaran, this.logo, this.statusPilih});

  ValuesMetodePembayaranModel.fromJson(Map<String, dynamic> json) {
    idMetodePembayaran = json['id_metode_pembayaran'];
    namaPembayaran = json['nama_pembayaran'];
    logo = json['logo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_metode_pembayaran'] = this.idMetodePembayaran;
    data['nama_pembayaran'] = this.namaPembayaran;
    data['logo'] = this.logo;
    return data;
  }
}