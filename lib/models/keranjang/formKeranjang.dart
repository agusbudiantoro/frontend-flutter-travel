class FormKeranjangModel {
  int idBarang;
  String namaBarang;
  String hargaBarang;
  int totalHarga;
  String hargaPromo;
  String deskripsi;
  int qty;
  String diskon;
  bool statusCeklis;
  String kategori;
  int stok;

  FormKeranjangModel({this.stok ,this.totalHarga,this.idBarang,this.deskripsi, this.hargaBarang, this.hargaPromo, this.namaBarang, this.qty, this.diskon, this.statusCeklis, this.kategori});
}