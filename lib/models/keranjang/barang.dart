class ListValueBarangKeranjang {
  List<ValuesListBarangKeranjang> value;

  ListValueBarangKeranjang({this.value});

  ListValueBarangKeranjang.fromJson(Map<String, dynamic> json) {
    if (json['value'] != null) {
      value = new List<ValuesListBarangKeranjang>();
      json['value'].forEach((v) {
        value.add(new ValuesListBarangKeranjang.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.value != null) {
      data['value'] = this.value.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ValuesListBarangKeranjang {
  int idBarang;
  int idKeranjang;
  String namaBarang;
  int qty;
  int hargaBarang;
  String gambar;
  int promo;
  String deskripsi;
  int hargaPromo;
  int kategori;
  int stok;
  int totalHarga;

  ValuesListBarangKeranjang(
      {this.idBarang,
      this.idKeranjang,
      this.namaBarang,
      this.qty,
      this.hargaBarang,
      this.gambar,
      this.promo,
      this.deskripsi,
      this.hargaPromo,
      this.kategori,
      this.stok,
      this.totalHarga});

  ValuesListBarangKeranjang.fromJson(Map<String, dynamic> json) {
    idKeranjang=json['id'];
    idBarang = json['id_barang'];
    namaBarang = json['nama_barang'];
    qty = json['qty'];
    hargaBarang = json['harga_barang'];
    gambar = json['gambar'];
    promo = json['promo'];
    deskripsi = json['deskripsi'];
    hargaPromo = json['harga_promo'];
    kategori = json['kategori'];
    totalHarga = json['total_harga'];
    stok = json['stok'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.idKeranjang;
    data['id_barang'] = this.idBarang;
    data['nama_barang'] = this.namaBarang;
    data['qty'] = this.qty;
    data['harga_arang'] = this.hargaBarang;
    data['gambar'] = this.gambar;
    data['promo'] = this.promo;
    data['deskripsi'] = this.deskripsi;
    data['harga_promo'] = this.hargaPromo;
    data['kategori'] = this.kategori;
    data['total_harga'] = this.totalHarga;
    data['stok'] = this.stok;
    return data;
  }
}