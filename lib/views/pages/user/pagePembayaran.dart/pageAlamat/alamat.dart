import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'dart:async';

import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:flutter_jamu_tujuh/models/alamatPengiriman/alamat.dart';

class GetLokasiForAlamat extends StatefulWidget {

  @override
  _GetLokasiForAlamatState createState() => _GetLokasiForAlamatState();
}

class _GetLokasiForAlamatState extends State<GetLokasiForAlamat> {
  var first;
  TextEditingController tambahanAlamat = TextEditingController();
  alamatPengirimanModel data;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: background1,
        automaticallyImplyLeading: false,
        actions: [
          Container(
            width: size.width,
            alignment: Alignment.centerLeft,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: captionColor,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                Center(
                  child: Text(
                    "Alamat Pengiriman",
                    style: TextStyle(fontSize: 14, color: captionColor),
                  ),
                ),
                IconButton(
                    icon: Icon(
                      Icons.send,
                      color: captionColor,
                    ),
                    onPressed: () {
                      Navigator.pop(context,data);
                    }),
              ],
            ),
          )
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(5),
        height: size.height,
        width: size.width,
        child: Column(
          children: [
            Card(
              child: Container(
                height: size.height/7,
                width: size.width,
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    Text(
                        (first == null)?"":"${first.featureName} : ${first.addressLine}",
                        style: TextStyle(fontFamily: 'RobotoCondensed'),
                        textAlign: TextAlign.left),                        
                  ],
                ),
              ),
            ),
            Card(
              child: Container(
                child: TextFormField(
                  onChanged: (val) {
                    setState(() {
                      data = alamatPengirimanModel(tambahanAlamat: val.toString(),fromGps: (first == null)?"":"${first.featureName} : ${first.addressLine}");
                    });
                  },
                  controller: tambahanAlamat,
                  keyboardType: TextInputType.multiline,
                  minLines: 1,
                  maxLines: 5,
                  cursorColor: Colors.black,
                  style: TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                      hintText: "Tambahan Alamat",
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black)),
                      hintStyle: TextStyle(color: Colors.black),
                      labelText: "Tambahan Alamat",
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      labelStyle: TextStyle(color: Colors.grey)),
                ),
              ),
            ),
            Card(
              child: Container(
                height: 50,
                width: size.width,
                child: IconButton(onPressed: () async{
                  Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
                  final coordinates = new Coordinates(position.latitude, position.longitude);
                  print(coordinates);
                  var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
                  setState(() {
                    first = addresses.first;
                  print("${first.featureName} : ${first.addressLine}");
                  data = alamatPengirimanModel(tambahanAlamat: tambahanAlamat.text.toString(),fromGps: (first == null)?"":"${first.featureName} : ${first.addressLine}");
                  });
                  // print(position.heading);
                  // getLokasi();
                }, icon: Icon(Icons.location_searching)),
              ),
            )
          ],
        ),
      ),
    );
  }
}