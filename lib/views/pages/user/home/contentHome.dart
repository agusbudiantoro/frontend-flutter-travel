import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_barang.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_listBarang.dart';
import 'package:flutter_jamu_tujuh/models/kategori/kategoriModel.dart';
import 'package:flutter_jamu_tujuh/models/modelsdummy/main.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/bloc_barang/barang_bloc.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/bloc_kategori/kategori_bloc.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/home/homeShimmer/shimmerbottomContainer.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/home/homeShimmer/shimmerhead_container.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/home/homeShimmer/shimmermiddle2Container.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/home/homeShimmer/shimmermiddle3Container.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/home/homeUser/bottomContainer.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/home/homeUser/head_container.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/home/homeUser/middle1Container.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/home/homeUser/middle2Container.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/home/homeUser/middle3Container.dart';
import 'package:flutter_jamu_tujuh/views/utils/custom_widgets/modalBuy/modalBuy.dart';
import 'package:shimmer/shimmer.dart';


class ContentHome extends StatefulWidget {
  @override
  _ContentHomeState createState() => _ContentHomeState();
}

class _ContentHomeState extends State<ContentHome> {
  int idKategori=0;
  KategoriBloc blocKategori = KategoriBloc();
  BarangBloc blocBarangRekom = BarangBloc();
  BarangBloc blocBarangPromo = BarangBloc();
  BarangBloc blocBarangSemua = BarangBloc();
  ValuesKategori valueKat=ValuesKategori(id: 0, namaKategori: "Semua");
  String cariBarang="";

  @override
  void initState() { 
    super.initState();
    blocBarangSemua..add(EventGetBarang());
    blocBarangRekom..add(EventGetBarangRekom());
    blocKategori..add(GetKategoriEvent());
    blocBarangPromo..add(EventGetBarangPromo());
  }

  doSomethingKetgori(ValuesKategori data) { 
    setState(() {
      valueKat=(data != null)?data:ValuesKategori(id: 0, namaKategori: "Semua");
    });
    print(valueKat.namaKategori);
    print(valueKat.id);
  }

  buttonBeliSekarang(BarangModel data){
    showModalBottomSheet(
      backgroundColor: colorGrey,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(88), topRight: Radius.circular(88))),
        context: context,
        builder: (context) {
          return ModalBuy(
            data: data,
          );
        });
  }

  buttonBeliMiddle3(BarangModel data){
    showModalBottomSheet(
      backgroundColor: colorGrey,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(88), topRight: Radius.circular(88))),
        context: context,
        builder: (context) {
          return ModalBuy(
            data: data,
          );
        });
  }

  buttonBeliBottom(BarangModel data){
    print(data);
    showModalBottomSheet(
      backgroundColor: colorGrey,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(88), topRight: Radius.circular(88))),
        context: context,
        builder: (context) {
          return ModalBuy(
            data: data,
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Column(
      children: [
        Flexible(
          child: Stack(
            children: [
              Container(
                color: circlePurpleDark,
              ),
              Container(
                decoration: BoxDecoration(
                    color: background1,
                    borderRadius:
                        BorderRadius.only(bottomLeft: Radius.circular(88))),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 24),
                          child: Column(
                            children: [
                              BlocBuilder<BarangBloc, BarangState>(
                                bloc: blocBarangSemua,
                                builder: (context, state) {
                                  if(state is StateGetBarangSukses){
                                    return HeadWidget(listRek: state.listData,callback: buttonBeliSekarang
                                    );
                                  }
                                  if(state is StateGetBarangWaiting){
                                    return ShimmerHeadWidget();
                                  }
                                  if(state is StateGetBarangFailed){
                                    return Container(child: Text("Gagal Mengambil Data"),);
                                  }
                                  return Container(child:Text("Gagal Ambil Data"));
                                },
                              ),
                              SizedBox(height: 10,),
                              Middle1Container(),
                              BlocBuilder<KategoriBloc, KategoriState>(
                                bloc: blocKategori,
                                builder: (context, state) {
                                  if (state is KategoriStateSukses) {
                                    return Middle2Container(listKat: state.myData,callback: doSomethingKetgori);
                                  }
                                  if (state is KategoriStateLoading) {
                                    return ShimmerMiddle2Container();
                                  }
                                  if (state is KategoriStateFailed) {
                                    return Container(
                                      child: Center(
                                        child: Text("Gagal Ambil Data Kategori"),
                                      ),
                                    );
                                  }
                                  return Container();
                                },
                              ),
                              BlocBuilder<BarangBloc, BarangState>(
                                bloc: blocBarangRekom,
                                builder: (context, state) {
                                  if(state is StateGetBarangRekomSukses){
                                    List<ValuesListBarang> listData = List<ValuesListBarang>();
                                    if(cariBarang.toString().length >0){
                                      if(valueKat.id == 0){
                                        listData = state.listData.where((i) => i.namaBarang.toString().toLowerCase().contains(cariBarang.toString().toLowerCase())).toList();
                                      } else if(valueKat.id != 0){
                                        listData = state.listData.where((i) => i.namaBarang.toString().toLowerCase().contains(cariBarang.toString().toLowerCase()) && i.kategori == valueKat.id).toList();
                                      }
                                    } else if(cariBarang.toString().length == 0){
                                      if(valueKat.id == 0){
                                        listData = state.listData;
                                      } else if(valueKat.id != 0){
                                        listData = state.listData.where((i) => i.kategori == valueKat.id).toList();
                                      }
                                    }
                                    if(listData.length != 0){
                                      return Middle3Container(listRek: listData,callback: buttonBeliMiddle3,);
                                    } else{
                                      return Container();
                                    }
                                  }
                                  if(state is StateGetBarangRekomWaiting){
                                    return ShimmerMiddle3Container();
                                  }
                                  if(state is StateGetBarangRekomFailed){
                                    return Container(child: Text("Gagal Mengambil Data"),);
                                  }
                                  return Container();
                                },
                              ),
                            ],
                          ),
                        ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
