import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/views/pages/user/feed/widget.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';



class FeedBarangPage extends StatefulWidget {
  final int jenis;

  FeedBarangPage({this.jenis});
  @override
  _FeedBarangPageState createState() => _FeedBarangPageState();
}

class _FeedBarangPageState extends State<FeedBarangPage> {

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: background1,
      body: IsiFeedBarang(jenis: widget.jenis,)
    );
  }
}
