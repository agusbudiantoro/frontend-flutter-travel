import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/views/utils/content/images.dart';

class WidgetPengiriman extends StatefulWidget {
  final double width;
  final double height;

  WidgetPengiriman({this.height, this.width});
  @override
  _WidgetPengirimanState createState() => _WidgetPengirimanState();
}

class _WidgetPengirimanState extends State<WidgetPengiriman> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.all(15),
      height: widget.height / 5.5,
      width: widget.width,
      color: Colors.white,
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 5),
            alignment: Alignment.centerLeft,
            child: Text(
              "Pengiriman",
              style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
          GestureDetector(
              child: Column(
            children: [
              Container(
                // alignment: Alignment.centerLeft,
                padding:
                    EdgeInsets.only(top: 15, bottom: 10, right: 5, left: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            alignment: Alignment.centerLeft,
                            child: new Text("KURIR",
                                style: TextStyle(fontFamily: 'RobotoCondensed'),
                                textAlign: TextAlign.left),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            child: Text("Rp 10.000",
                                style: TextStyle(fontFamily: 'RobotoCondensed', fontSize: 16, fontWeight: FontWeight.bold, color: Colors.red),
                                textAlign: TextAlign.left),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Container(
                      height: 20,
                      width: widget.width / 4.5,
                      // color: Colors.red,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          // Container(
                          //   height: 20,
                          //   width: widget.width / 7,
                          //   child: new Image.asset(
                          //     logoGrab,
                          //     // scale: 2,
                          //     fit: BoxFit.contain,
                          //   ),
                          // ),
                          SizedBox(
                            width: 5,
                          ),
                          Icon(Icons.arrow_forward_ios_sharp),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding:
                    EdgeInsets.only(top: 15, bottom: 10, right: 5, left: 5),
                child: Container(
                  height: 1,
                  width: widget.width,
                  color: Colors.grey.withOpacity(0.3),
                ),
              )
            ],
          )),
        ],
      ),
    );
  }
}
