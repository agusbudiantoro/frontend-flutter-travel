import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_jamu_tujuh/models/kategori/kategoriModel.dart';
import 'package:flutter_jamu_tujuh/view_models/bloc/bloc_kategori/kategori_bloc.dart';
import 'package:shimmer/shimmer.dart';

import '../../../colors.dart';

typedef GetKatgori = void Function(ValuesKategori);
class ShimmerMiddle2Container extends StatefulWidget {
  List<ValuesKategori> listKat;
  final GetKatgori callback;
  ShimmerMiddle2Container({this.listKat, this.callback});
  @override
  _ShimmerMiddle2ContainerState createState() =>
      _ShimmerMiddle2ContainerState(listKat: this.listKat);
}

class _ShimmerMiddle2ContainerState extends State<ShimmerMiddle2Container> {
  List<ValuesKategori> listKat;
  _ShimmerMiddle2ContainerState({this.listKat});
  int idKategori = 0;
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(top: 10, bottom: 10),
          height: 50,
          // width: size.width - (size.width / 5.5),
          child: Row(
            children: [
              Shimmer.fromColors(
                baseColor: Colors.grey,
                highlightColor: Colors.white,
                child: GestureDetector(
                  child: Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(left: 10,right: 10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4), color: Colors.red),
                    child: Text(
                      "Semua",
                      style: TextStyle(
                          color: background1,
                          fontWeight: FontWeight.bold,
                          fontSize: 12),
                    ),
                  ),
                  onTap: (){
                    
                  },
                ),
              ),
              pilihKategori(listKat)
            ],
          ));
  }

  Flexible pilihKategori(List<ValuesKategori> data) {
    return Flexible(
      child: ListView.builder(
        padding: EdgeInsets.all(10),
        shrinkWrap: true,
        itemCount: 2,
        scrollDirection: Axis.horizontal,
        physics: AlwaysScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int i) {
          return Shimmer.fromColors(
            baseColor: Colors.grey,
            highlightColor: Colors.white,
            child: GestureDetector(
                child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(left:10, right: 10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4), color: Colors.red),
                ),
                onTap: (){
                  
                },
              ),
          );
        },
      ),
    );
  }
}
