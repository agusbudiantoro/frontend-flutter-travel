import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerMiddle1Container extends StatefulWidget {
  @override
  _ShimmerMiddle1ContainerState createState() => _ShimmerMiddle1ContainerState();
}

class _ShimmerMiddle1ContainerState extends State<ShimmerMiddle1Container> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Shimmer.fromColors(
                  baseColor: Colors.grey,
                  highlightColor: Colors.white,
                  child: Container(
                    child: Text(
                      "Produk Kami",
                      style: TextStyle(
                          color: circlePurpleDark,
                          fontWeight: FontWeight.w600,
                          fontSize: 20),
                    ),
                  ),
                ),
                Shimmer.fromColors(
                  baseColor: Colors.grey,
                  highlightColor: Colors.white,
                  child: Container(
                    width: 40,
                    height: 40,
                    decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(8)
                    ),
                    child: IconButton(
                      focusColor: Colors.yellow,
                      color: background1,
                      onPressed: (){},
                      icon: Icon(Icons.list)),
                  ),
                )
              ],
            ),
          )
    );
  }
}
