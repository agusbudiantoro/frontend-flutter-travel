import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';

class BottomBarHome extends StatefulWidget {
  final Function(int) selectPage;
  final int page;
  BottomBarHome({this.selectPage, this.page});
  @override
  _BottomBarHomeState createState() => _BottomBarHomeState();
}

class _BottomBarHomeState extends State<BottomBarHome> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left:20,right:20),
      height: 62,
      decoration: BoxDecoration(color: background1, boxShadow: [
        BoxShadow(
            offset: Offset(0, -10),
            blurRadius: 35,
            color: Colors.white.withOpacity(0.30))
      ]),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
              children: [
                IconButton(icon: Icon((widget.page == 0)?Icons.home:Icons.home_outlined, color: Colors.white,), iconSize: 30, onPressed: () {
                  widget.selectPage(0);
                }),
                Text("Beranda", style: TextStyle(color: Colors.white, fontSize: 12, fontWeight: (widget.page == 0)?FontWeight.bold:FontWeight.normal),)
              ],
            ),
          Column(
            children: [
              IconButton(icon: Icon((widget.page == 1)?Icons.shopping_cart_rounded:Icons.shopping_cart_outlined,color: Colors.white), iconSize: 30, onPressed: () {
                widget.selectPage(1);
              }),
              Text("Keranjang", style: TextStyle(color: Colors.white, fontSize: 12, fontWeight: (widget.page == 1)?FontWeight.bold:FontWeight.normal),)
            ],
          ),
          Column(
            children: [
              IconButton(icon: Icon(Icons.transfer_within_a_station,color: Colors.white,), iconSize: (widget.page == 2)?31:30, onPressed: () {
                widget.selectPage(2);
              }),
              Text("Pembayaran", style: TextStyle(color: Colors.white, fontSize: 12, fontWeight: (widget.page == 2)?FontWeight.bold:FontWeight.normal),)
            ],
          ),
          Column(
            children: [
              IconButton(icon: Icon((widget.page == 3)?Icons.person:Icons.person_outline,color: Colors.white), iconSize: 30, onPressed: () {
                widget.selectPage(3);
              }),
              Text("Akun", style: TextStyle(color: Colors.white, fontSize: 12, fontWeight: (widget.page == 3)?FontWeight.bold:FontWeight.normal),)
            ],
          )
        ],
      ),
    );
  }
}
