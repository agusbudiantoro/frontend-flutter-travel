import 'package:flutter/material.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_barang.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_listBarang.dart';
import 'package:flutter_jamu_tujuh/models/modelsdummy/main.dart';
import 'package:flutter_jamu_tujuh/view_models/networkApi/domain.dart';
import 'package:flutter_jamu_tujuh/views/utils/colors.dart';
typedef GetBarang = void Function(BarangModel);
class HeadWidgetAdmin extends StatelessWidget {
  List<ValuesListBarang> listRek;
  final GetBarang callback;
  HeadWidgetAdmin({this.listRek, this.callback});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height/4.5,
      child: Container(
        width: size.width,
        // color: Colors.white,
        child: Card(
                        elevation: 0,
                        clipBehavior: Clip.antiAlias,
                        shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
                        child: Container(
                          decoration: BoxDecoration(
                              color: contentColor,
                              borderRadius: BorderRadius.all(Radius.circular(30)),
                              ),
                          // padding: EdgeInsets.all(10),
                          height: size.height / 4,
                          width: size.width / 2.4,
                          child: Stack(
                            children: [
                              Container(
                                width: size.width,
                                height: (size.height / 4),
                                child: new Image.network(domainurl+'/gambar/'+listRek[0].gambar.toString(),
                                  fit: BoxFit.cover,
                                ),
                              ),
                              Positioned(
                                // left: 20,
                                bottom: 0,
                                child: Container(
                                  height: 80,
                                  width: size.width,
                                  color: Colors.black.withOpacity(0.3),
                                  child: Column(
                                    children: [
                                      SizedBox(height: 5,),
                                      Container(
                                        padding: EdgeInsets.all(10),
                                        alignment: Alignment.centerLeft,
                                        child: Text(listRek[0].namaBarang.toString(), style: TextStyle(color: Colors.white,fontSize: 15,fontWeight: FontWeight.bold),),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(left:10),
                                        alignment: Alignment.centerLeft,
                                        child: Text((listRek[0].promo == 0)?"IDR "+listRek[0].harga.toString():"IDR "+listRek[0].hargaPromo.toString(), style: TextStyle(color: captionColor, fontWeight: FontWeight.w600),),
                                      )
                                    ],
                                  ),
                                )
                                ),
                                Positioned(
                              top: 80,
                              left: 0,
                              child: Container(
                                  alignment: Alignment.center,
                                  width: 100,
                                  height: 20,
                                  transform: Matrix4.rotationZ(-1.58),
                                  color: Colors.red,
                                  child: Text(
                                    "NEW",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  )),
                            ),
                              (listRek[0].promo != 0)?Positioned(
                                top: -20,
                                right: -45,
                                child: Container(
                                    alignment: Alignment.center,
                                    width: 100,
                                    height: 20,
                                    transform: Matrix4.rotationZ(0.8),
                                    color: Colors.red,
                                    child: Text(
                                      listRek[0].promo.toString()+"%",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    )),
                              ):Container(),
                            ],
                          ),
                        ),
                      ),
      ),
    );
  }
}
