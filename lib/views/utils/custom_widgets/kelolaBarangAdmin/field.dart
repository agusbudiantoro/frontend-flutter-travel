import 'package:flutter/material.dart';


class TexFieldCustom extends StatefulWidget {
String containerField;
TextEditingController isi = TextEditingController();
final VoidCallback tapCallBack;
final VoidCallback onChangeCall;
bool typeNumber;
bool readOnly;

TexFieldCustom({this.containerField, this.isi, this.tapCallBack, this.readOnly, this.typeNumber, this.onChangeCall});
  @override
  _TexFieldCustomState createState() => _TexFieldCustomState();
}

class _TexFieldCustomState extends State<TexFieldCustom> {
  @override
  Widget build(BuildContext context) {
    return Container(
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                        flex: 2,
                        child: TextFormField(
                          onChanged: (val){
                            if(widget.onChangeCall != null){
                              widget.onChangeCall();
                            }
                          },
                          readOnly: widget.readOnly,
                          onTap: (){
                            if(widget.tapCallBack != null){
                              widget.tapCallBack();
                            }
                          },
                          controller: widget.isi,
                          keyboardType: (widget.typeNumber == true)?TextInputType.number: TextInputType.multiline,
                          minLines: 1,
                          maxLines: 5,
                          cursorColor: Colors.black,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                              hintText: widget.containerField,
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black)),
                              hintStyle: TextStyle(color: Colors.black),
                              labelText: widget.containerField,
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                              ),
                              labelStyle: TextStyle(color: Colors.grey)),
                        )),
                  ],
                ),
            );
  }
}