import 'package:flutter/material.dart';

class PageNoPesanan extends StatefulWidget {
  final String noPesanan;
  PageNoPesanan({this.noPesanan});
  @override
  _PageNoPesananState createState() => _PageNoPesananState();
}

class _PageNoPesananState extends State<PageNoPesanan> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      color: Colors.grey[350],
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text("NO PESANAN", style: TextStyle(color: Colors.grey, fontSize:10),),
          Text(widget.noPesanan.toString(),style: TextStyle(color: Colors.black, fontSize: 12, fontWeight: FontWeight.bold),)
        ],
      ),
    );
  }
}