part of 'pesanan_bloc.dart';

@immutable
abstract class PesananState {}

class PesananInitial extends PesananState {}

class StatePostPesananSukses extends PesananState {
  
}

class StatePostPesananWaiting extends PesananState{
}

class StatePostPesananFailed extends PesananState {
  final String errorMessage;
  StatePostPesananFailed({this.errorMessage});
}

// get all pesanan
class StateGetAllPesananSukses extends PesananState{
  List<ValueGetPemesanan> data;
  StateGetAllPesananSukses({this.data});
}

class StateGetAllPesananWaiting extends PesananState {
  
}

class StateGetAllPesananFailed extends PesananState {
  final String errorMessage;
  StateGetAllPesananFailed({this.errorMessage});
}

// deletel pesanan
class StateDeletePesananSukses extends PesananState{
  
}

class StateDeletePesananWaiting extends PesananState {
  
}

class StateDeleteFailed extends PesananState {
  final String errorMessage;
  StateDeleteFailed({this.errorMessage});
}

// put status pesanan
class StatePutStatusPesananSukses extends PesananState{
  
}

class StatePutStatusPesananWaiting extends PesananState {
  
}

class StatePutStatusFailed extends PesananState {
  final String errorMessage;
  StatePutStatusFailed({this.errorMessage});
}