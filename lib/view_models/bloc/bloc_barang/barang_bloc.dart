import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_barang.dart';
import 'package:flutter_jamu_tujuh/models/barang/model_listBarang.dart';
import 'package:flutter_jamu_tujuh/view_models/networkApi/barang/barangApi.dart';

part 'barang_event.dart';
part 'barang_state.dart';

class BarangBloc extends Bloc<BarangEvent, BarangState> {
  BarangBloc() : super(BarangInitial());

  @override
  Stream<BarangState> mapEventToState(
    BarangEvent event,
  ) async* {
    if(event is EventPostBarang){
      yield* _postBarang(event.data);
    }
    if(event is EventGetBarang){
      yield* _getBarang();
    }
    if(event is EventPutBarang){
      yield* _putBarang(event.data);
    }
    if(event is EventDelBarang){
      yield* _delBarang(event.data);
    }
    if(event is EventGetBarangPromo){
      yield* _getBarangPromo();
    }
    if(event is EventGetBarangRekom){
      yield* _getBarangRekom();
    }
  }
}

Stream<BarangState> _getBarangRekom()async*{
  yield StateGetBarangRekomWaiting();
  try {
    dynamic res = await BarangApi.getBarangRekom();
    var isiJson = jsonDecode(res);
    ListBarangModel hasilConvert = ListBarangModel.fromJson(isiJson);
    List<ValuesListBarang> hasilBarang = hasilConvert.values;
    yield StateGetBarangRekomSukses(listData: hasilBarang);
  } catch (e) {
    yield StateGetBarangRekomFailed(errorMessage: e.toString());
  }
}

Stream<BarangState> _getBarangPromo()async*{
  yield StateGetBarangPromoWaiting();
  try {
    dynamic res = await BarangApi.getBarangPromo();
    var isiJson = jsonDecode(res);
    ListBarangModel hasilConvert = ListBarangModel.fromJson(isiJson);
    List<ValuesListBarang> hasilBarang = hasilConvert.values;
    yield StateGetBarangPromoSukses(listData: hasilBarang);
  } catch (e) {
    yield StateGetBarangPromoFailed(errorMessage: e.toString());
  }
}

Stream<BarangState> _delBarang(data)async*{
  yield StateDelBarangWaiting();
  try {
    dynamic res = await BarangApi.delBarang(data);
    yield StateDelBarangSukses();
  } catch (e) {
    yield StateDelBarangFailed(errorMessage: e.toString());
  }
}

Stream<BarangState> _getBarang()async*{
  yield StateGetBarangWaiting();
  try {
    dynamic res = await BarangApi.getBarang();
    var isiJson = jsonDecode(res);
    ListBarangModel hasilConvert = ListBarangModel.fromJson(isiJson);
    List<ValuesListBarang> hasilBarang = hasilConvert.values;
    yield StateGetBarangSukses(listData: hasilBarang);
  } catch (e) {
    yield StateGetBarangFailed(errorMessage: e.toString());
  }
}

Stream<BarangState> _postBarang(data)async*{
  yield StatePostBarangWaiting();
  try {
    dynamic res = await BarangApi.postBarang(data);
    yield StatePostBarangSukses();
  } catch (e) {
    yield StateGetBarangFailed(errorMessage: e.toString());
  }
}

Stream<BarangState> _putBarang(data)async*{
  yield StatePutBarangWaiting();
  try {
    dynamic res = await BarangApi.putBarang(data);
    yield StatePutBarangSukses();
  } catch (e) {
    yield StatePutBarangFailed(errorMessage: e.toString());
  }
}