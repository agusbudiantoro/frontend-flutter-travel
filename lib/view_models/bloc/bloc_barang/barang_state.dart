part of 'barang_bloc.dart';

@immutable
abstract class BarangState {}

class BarangInitial extends BarangState {}

// get
class StateGetBarangSukses extends BarangState {
  List<ValuesListBarang> listData;
  StateGetBarangSukses({this.listData});
}

class StateGetBarangWaiting extends BarangState{
  
}

class StateGetBarangFailed extends BarangState {
  String errorMessage;
  StateGetBarangFailed({this.errorMessage});
}

// post
class StatePostBarangSukses extends BarangState {
  
}

class StatePostBarangWaiting extends BarangState{
  
}

class StatePostBarangFailed extends BarangState {
  String errorMessage;
  StatePostBarangFailed({this.errorMessage});
}

// put
class StatePutBarangSukses extends BarangState {
  
}

class StatePutBarangWaiting extends BarangState{
  
}

class StatePutBarangFailed extends BarangState {
  String errorMessage;
  StatePutBarangFailed({this.errorMessage});
}

// delete
class StateDelBarangSukses extends BarangState {
  
}

class StateDelBarangWaiting extends BarangState{
  
}

class StateDelBarangFailed extends BarangState {
  String errorMessage;
  StateDelBarangFailed({this.errorMessage});
}

// get barang rekom
class StateGetBarangRekomSukses extends BarangState {
  List<ValuesListBarang> listData;
  StateGetBarangRekomSukses({this.listData});
}

class StateGetBarangRekomWaiting extends BarangState{
  
}

class StateGetBarangRekomFailed extends BarangState {
  String errorMessage;
  StateGetBarangRekomFailed({this.errorMessage});
}

// get barang promo
class StateGetBarangPromoSukses extends BarangState {
  List<ValuesListBarang> listData;
  StateGetBarangPromoSukses({this.listData});
}

class StateGetBarangPromoWaiting extends BarangState{
  
}

class StateGetBarangPromoFailed extends BarangState {
  String errorMessage;
  StateGetBarangPromoFailed({this.errorMessage});
}