part of 'bloc_login_bloc.dart';

@immutable
abstract class BlocLoginState {}

class BlocLoginInitial extends BlocLoginState {}

class BlocStateSukses extends BlocLoginState{
  final MyDataLogin myData;
    BlocStateSukses({this.myData});
}

class BlocaStateFailed extends BlocLoginState{
  final String errorMessage;
  BlocaStateFailed({this.errorMessage});
}

class BlocStateLoading extends BlocLoginState{

}

class BlocStateCekTokenSukses extends BlocLoginState{
  final MyDataLogin data;
  BlocStateCekTokenSukses({this.data});
}

class BlocaStateCekTokenFailed extends BlocLoginState{
  final String errorMessage;
  BlocaStateCekTokenFailed({this.errorMessage});
}

class BlocStateRegistLoading extends BlocLoginState{

}

class BlocStateRegistSukses extends BlocLoginState{
  
}

class BlocaStateRegistFailed extends BlocLoginState{
  final String errorMessage;
  BlocaStateRegistFailed({this.errorMessage});
}

class BlocStateEditPassLoading extends BlocLoginState{

}

class BlocStateEditPassSukses extends BlocLoginState{
  
}

class BlocaStateEditPassFailed extends BlocLoginState{
  final String errorMessage;
  BlocaStateEditPassFailed({this.errorMessage});
}