part of 'barangkeranjang_bloc.dart';

abstract class BarangkeranjangEvent {}

class EventPostBarangKeranjang extends BarangkeranjangEvent {
  FormKeranjangModel data;
  EventPostBarangKeranjang({this.data});
}

class EventGetBarangKeranjangByIdKonsumen extends BarangkeranjangEvent {
  
}

class EventDelBarangKeranjang extends BarangkeranjangEvent{
  int id;
  EventDelBarangKeranjang({this.id});
}