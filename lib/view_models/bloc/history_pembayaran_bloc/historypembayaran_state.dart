part of 'historypembayaran_bloc.dart';

@immutable
abstract class HistorypembayaranState {}

class HistorypembayaranInitial extends HistorypembayaranState {}

class StateHistoryPembayaranSukses extends HistorypembayaranState{
  List<ValueGetPemesanan> data;
  StateHistoryPembayaranSukses({this.data});
}

class StateHistoryPembayaranWaiting extends HistorypembayaranState {
  
}

class StateHistoryPembayaranFailed extends HistorypembayaranState {
  final String errorMessage;
  StateHistoryPembayaranFailed({this.errorMessage});
}