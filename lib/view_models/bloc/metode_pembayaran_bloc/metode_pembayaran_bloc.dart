import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:flutter_jamu_tujuh/models/metodePembayaran/metodePembayaran.dart';
import 'package:flutter_jamu_tujuh/view_models/networkApi/MetodePembayaran/metodePembayaranApi.dart';

part 'metode_pembayaran_event.dart';
part 'metode_pembayaran_state.dart';

class MetodePembayaranBloc extends Bloc<MetodePembayaranEvent, MetodePembayaranState> {
  MetodePembayaranBloc() : super(MetodePembayaranInitial());

  @override
  Stream<MetodePembayaranState> mapEventToState(
    MetodePembayaranEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if(event is EventGetMetodePembayaran){
      yield* _getMetodePembayaran();
    }
  }
}

Stream<MetodePembayaranState> _getMetodePembayaran()async*{
  yield StateMetodePembayaranWaiting();
  try {
      dynamic value = await MetodePembayaranApi.getMetodePembayaran();
      var hasil = jsonDecode(value);
      metodePembayaranModel hasilConvert = metodePembayaranModel.fromJson(hasil);
      List<ValuesMetodePembayaranModel> hasilMetodePembayaran = hasilConvert.values;
    yield StateMetodePembayaranSukses(data:hasilMetodePembayaran);
  } catch (e) {
    yield StateMetodePembayaranFailed(errorMessage: e.toString());
  }
}