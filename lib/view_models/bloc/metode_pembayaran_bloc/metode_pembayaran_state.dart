part of 'metode_pembayaran_bloc.dart';

@immutable
abstract class MetodePembayaranState {}

class MetodePembayaranInitial extends MetodePembayaranState {}

class StateMetodePembayaranSukses extends MetodePembayaranState {
  final List<ValuesMetodePembayaranModel> data;
  StateMetodePembayaranSukses({this.data});
}

class StateMetodePembayaranFailed extends MetodePembayaranState {
  final String errorMessage;
  StateMetodePembayaranFailed({this.errorMessage});
}

class StateMetodePembayaranWaiting extends MetodePembayaranState {
  
}